# How to install and setup MetaMask

## Installing the extension

The first thing you need to do is install the add-on MetaMask in your browser. To do this, you need to log into your Google account. Supported browsers to MetaMask are Firefox and Chrome. We are covering the installation on Chrome: click [here](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en) to open Chrome Web Store and pick the MetaMask extension. Click *Add* to Chrome and wait for the download to finish.

## Account creation

After installing the add-on, you will be presented with a page for connecting an existing account or creating a new one.
Select *create an account*, agree to the proposed agreements, and create your password to access your account. You will be prompted to view the seed phrase. Please store it in a safe way, as it will be required for you to back up or restore your account.

## Network configuration

You need to configure your newly created wallet to support and accept Q tokens. To do this, you need to connect to the Q blockchain. On the main page of your account, select *Custom RPC* in the upper right corner in the list of networks. Then enter the following details to setup the connection to Q:

| **Parameter** | **Value** |
|:--|:--|
| Networkname | Q Testnet|
| RPC-URL | https://rpc.qtestnet.org |
| Chain-ID | 35443 |
| Symbol | Q |
| Block-Explorer-URL| https://explorer.qtestnet.org |

Note: these parameters are valid for Q testnet only!

## Faucet

To continue working on testnet, you need to receive some Q tokens to your address using the faucet. Add your wallet address (something like *0x0AbC123...abc*) into the below listet URL of the faucet and open the URL within your browser.  

`https://faucet.qtestnet.org/<wallet address>`  

If the faucet is working as expected, your balance will increase within a couple of seconds.

Other assets on Q testnet are be supported by the faucet as well. The asset type simply needs to be added to the URL like a sub-folder before the wallet address:

`https://faucet.qtestnet.org/QBTC/<wallet address>`  
`https://faucet.qtestnet.org/QUSD/<wallet address>`
